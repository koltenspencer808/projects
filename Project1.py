import numpy as np                      #imports numpy which is a base package
from scipy.integrate import odeint      #imports odeint function so we have type scipy.integrate.odeint
import matplotlib.pyplot as plt         #imports pyplot for plotting and graphing

# model function that returns dx/dt
def model(x,t,k):
    dxdt = k * t
    return dxdt

# initial condition
x0 = 30

# time points
t = np.linspace(0,120,100)              #time input points from 0-120 (start,end, number of points)

# solve ODE that accepts multiple value of k
k = 0.001
x1 = odeint(model,x0,t,args=(k,))
k = 0.0025
x2 = odeint(model,x0,t,args=(k,))
k = 0.0045
x3 = odeint(model,x0,t,args=(k,))

# plot results
plt.plot(t,x1,'r-',linewidth=2,label='k=10%')           #plots x1
plt.plot(t,x2,'b--',linewidth=2,label='k=25%')          #plots x2
plt.plot(t,x3,'g:',linewidth=2,label='k=45%')           #plots x3
plt.xlabel('Time (Mins)')                               #labels the x axis
plt.ylabel('Temperature (C)')                           #labels the y axis
plt.legend(loc='best')                                  #creates the legend in the best location
plt.show()   